# Restre

Restre is an utility file for making mods with [Risk of Rain Modloader](https://rainfusion.ml).
The file provides alternative functions for 'require' and similar functions to work for relative directories instead of being based on the mod root directory.
It also gives you a way to check for dependencies and give a warning or an error if they're not met.

Load the file using:
```lua
require("restre")()
```

If you want to be more complete (use nil for submodfolder if you're at the mod root):
```lua
local restre_key
if (not restre) or (not restre.valid()) then
    restre_key = require("restre")()
    restre.cd("submodfolder/", "main")
end
```

Note that Restre is not a mod and has main.lua and metadata.json only so that it can be displayed as one.
