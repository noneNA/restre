-- Restre - main.lua

local restre_key
if (not restre) or (not restre.valid()) then
    restre_key = require("restre")()
    restre.cd("submodfolder/", "main")
end
